package ru.ivmiit.fantasysport.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.ivmiit.fantasysport.dao.interfaces.TeamsDao;
import ru.ivmiit.fantasysport.models.Team;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class TeamsDaoImpl implements TeamsDao {

    //language=SQL
    private static final String SQL_FIND_ALL_TEAMS = "SELECT * FROM team";
    //language=SQL
    private static final String SQL_SAVE_TEAM = "INSERT INTO team (name, country, rating) " +
            "VALUES (:name_, :country, :rating)";
    //language=SQL
    private static final String SQL_UPDATE_TEAM = "UPDATE team SET name = :name_, " +
            "country = :country, rating = :rating WHERE id = :id";
    //language=SQL
    private static final String SQL_DELETE_TEAM = "DELETE FROM team WHERE id = :id";


    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public TeamsDaoImpl(DataSource dataSource) {
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public List<Team> findAll() {
        return namedParameterJdbcTemplate.query(SQL_FIND_ALL_TEAMS,
                (resultSet, i) -> new Team.Builder()
                        .id(resultSet.getLong("id"))
                        .name(resultSet.getString("name"))
                        .country(resultSet.getString("country"))
                        .rating(resultSet.getDouble("rating"))
                        .build());
    }

    @Override
    public void save(Team team) {
        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("name_", team.getName());
        namedParameters.put("country", team.getCountry());
        namedParameters.put("rating", team.getRating());
        namedParameterJdbcTemplate.update(SQL_SAVE_TEAM, namedParameters);
    }

    @Override
    public void update(Team team) {
        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("name_", team.getName());
        namedParameters.put("country", team.getCountry());
        namedParameters.put("rating", team.getRating());
        namedParameters.put("id", team.getId());
        namedParameterJdbcTemplate.update(SQL_UPDATE_TEAM, namedParameters);
    }

    @Override
    public void delete(long id) {
        Map<String, Long> namedParameters = new HashMap<>();
        namedParameters.put("id", id);
        namedParameterJdbcTemplate.update(SQL_DELETE_TEAM, namedParameters);
    }
}
