package ru.ivmiit.fantasysport.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.ivmiit.fantasysport.dao.interfaces.PlayersDao;
import ru.ivmiit.fantasysport.models.Player;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class PlayersDaoImpl implements PlayersDao {

    //language=SQL
    private static final String SQL_FIND_CHESS_PLAYERS = "SELECT p.* FROM player p, chess_player c WHERE p.id = c.player_id";
    //language=SQL
    private static final String SQL_FIND_TENNIS_PLAYERS = "SELECT p.* FROM player p, tennis_player t WHERE p.id = t.player_id";
    //language=SQL
    private static final String SQL_FIND_CHESS_PLAYER_BY_NAME = "SELECT p.* FROM player p, chess_player c WHERE name = :name_ AND p.id = c.player_id";
    //language=SQL
    private static final String SQL_FIND_TENNIS_PLAYER_BY_NAME = "SELECT p.* FROM player p, tennis_player t WHERE name = :name_ AND p.id = t.player_id";
    //language=SQL
    private static final String SQL_SAVE_PLAYER = "INSERT INTO player (name, age, country, rating, type) " +
            "VALUES (:name_, :age, :country, :rating, :type_)";
    //language=SQL
    private static final String SQL_UPDATE_PLAYER = "UPDATE player SET name = :name_, " +
            "age = :age, country = :country, rating = :rating WHERE id = :id";
    //language=SQL
    private static final String SQL_DELETE_PLAYER = "DELETE FROM player WHERE id = :id";

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public PlayersDaoImpl(DataSource dataSource) {
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public List<Player> findAllChessPlayers() {
        return namedParameterJdbcTemplate.query(SQL_FIND_CHESS_PLAYERS,
                (resultSet, i) -> new Player.Builder()
                        .id(resultSet.getLong("id"))
                        .name(resultSet.getString("name"))
                        .age(resultSet.getInt("age"))
                        .country(resultSet.getString("country"))
                        .rating(resultSet.getDouble("rating"))
                        .type(resultSet.getString("type"))
                        .build());
    }

    @Override
    public List<Player> findAllTennisPlayers() {
        return namedParameterJdbcTemplate.query(SQL_FIND_TENNIS_PLAYERS,
                (resultSet, i) -> new Player.Builder()
                        .id(resultSet.getLong("id"))
                        .name(resultSet.getString("name"))
                        .age(resultSet.getInt("age"))
                        .country(resultSet.getString("country"))
                        .rating(resultSet.getDouble("rating"))
                        .type(resultSet.getString("type"))
                        .build());
    }

    @Override
    public Player findChessPlayerByName(String name) {
        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("name_", name);
        return namedParameterJdbcTemplate.queryForObject(SQL_FIND_CHESS_PLAYER_BY_NAME, namedParameters,
                (resultSet, i) -> new Player.Builder()
                        .id(resultSet.getLong("id"))
                        .name(resultSet.getString("name"))
                        .age(resultSet.getInt("age"))
                        .country(resultSet.getString("country"))
                        .rating(resultSet.getDouble("rating"))
                        .type(resultSet.getString("type"))
                        .build());
    }

    @Override
    public Player findTennisPlayerByName(String name) {
        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("name_", name);
        return namedParameterJdbcTemplate.queryForObject(SQL_FIND_TENNIS_PLAYER_BY_NAME, namedParameters,
                (resultSet, i) -> new Player.Builder()
                        .id(resultSet.getLong("id"))
                        .name(resultSet.getString("name"))
                        .age(resultSet.getInt("age"))
                        .country(resultSet.getString("country"))
                        .rating(resultSet.getDouble("rating"))
                        .type(resultSet.getString("type"))
                        .build());
    }

    @Override
    public void save(Player player) {
        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("name_", player.getName());
        namedParameters.put("age", player.getAge());
        namedParameters.put("country", player.getCountry());
        namedParameters.put("rating", player.getRating());
        namedParameters.put("type_", player.getType());
        namedParameterJdbcTemplate.update(SQL_SAVE_PLAYER, namedParameters);
    }

    @Override
    public void update(Player player) {
        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("name_", player.getName());
        namedParameters.put("age", player.getAge());
        namedParameters.put("country", player.getCountry());
        namedParameters.put("rating", player.getRating());
        namedParameters.put("id", player.getId());
        namedParameterJdbcTemplate.update(SQL_UPDATE_PLAYER, namedParameters);
    }

    @Override
    public void delete(long id) {
        Map<String, Long> namedParameters = new HashMap<>();
        namedParameters.put("id", id);
        namedParameterJdbcTemplate.update(SQL_DELETE_PLAYER, namedParameters);
    }
}
