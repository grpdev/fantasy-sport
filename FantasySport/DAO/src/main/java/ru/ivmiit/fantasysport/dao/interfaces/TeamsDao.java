package ru.ivmiit.fantasysport.dao.interfaces;

import ru.ivmiit.fantasysport.models.Team;

import java.util.List;

public interface TeamsDao {

    List<Team> findAll();
    void save(Team team);
    void update(Team team);
    void delete(long id);
}
