package ru.ivmiit.fantasysport.dao.interfaces;

import ru.ivmiit.fantasysport.models.Player;

import java.util.List;

public interface PlayersDao {

    List<Player> findAllChessPlayers();
    List<Player> findAllTennisPlayers();
    Player findChessPlayerByName(String name);
    Player findTennisPlayerByName(String name);
    void save(Player player);
    void update(Player player);
    void delete(long id);
}
