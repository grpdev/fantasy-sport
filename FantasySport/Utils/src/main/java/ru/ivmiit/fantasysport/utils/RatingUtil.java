package ru.ivmiit.fantasysport.utils;

public interface RatingUtil {

    void downloadChessRating();
    void readChessRating();
    void importTennisRating();
    void importFootballRating();
}
