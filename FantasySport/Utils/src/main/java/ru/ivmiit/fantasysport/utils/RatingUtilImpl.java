package ru.ivmiit.fantasysport.utils;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import ru.ivmiit.fantasysport.models.Player;
import ru.ivmiit.fantasysport.models.Team;
import ru.ivmiit.fantasysport.service.interfaces.PlayerService;
import ru.ivmiit.fantasysport.service.interfaces.TeamService;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.Calendar;

import static org.jsoup.Jsoup.connect;

@Component
@EnableScheduling
public class RatingUtilImpl implements RatingUtil {

    @Autowired
    private PlayerService playerService;
    @Autowired
    private TeamService teamService;

    @Override
    //@Scheduled(fixedDelay = 2592000000L)
    public void downloadChessRating() {
        try {
            URL website = new URL("http://ratings.fide.com/download/blitz_rating_list_xml.zip");
            ReadableByteChannel rbc = Channels.newChannel(website.openStream());
            FileOutputStream fos = new FileOutputStream("/home/dmitry/IdeaProjects/fantasy-sport/FantasySport/Utils/src/main/resources/ratings/rating.zip");
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
            ZipFile zipFile = new ZipFile("/home/dmitry/IdeaProjects/fantasy-sport/FantasySport/Utils/src/main/resources/ratings/rating.zip");
            zipFile.extractAll("/home/dmitry/IdeaProjects/fantasy-sport/FantasySport/Utils/src/main/resources/ratings");
            readChessRating();
        } catch (IOException | ZipException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void readChessRating() {
        try {
            DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = documentBuilder.parse("/home/dmitry/IdeaProjects/fantasy-sport/FantasySport/Utils/src/main/resources/ratings/blitz_rating_list.xml");

            Node root = document.getDocumentElement();

            NodeList players = root.getChildNodes();
            for (int i = 0; i < players.getLength(); i++) {
                NodeList player = players.item(i).getChildNodes();
                Player chessPlayer = new Player.Builder().build();
                for(int j = 0; j < player.getLength(); j++) {
                    Node info = player.item(j);
                    if(info.getNodeName().equals("name"))
                        chessPlayer.setName(info.getTextContent());
                    if(info.getNodeName().equals("birthday") && !info.getTextContent().equals(""))
                        chessPlayer.setAge(Calendar.getInstance().get(Calendar.YEAR) - Integer.parseInt(info.getTextContent()));
                    if(info.getNodeName().equals("country"))
                        chessPlayer.setCountry(info.getTextContent());
                    if(info.getNodeName().equals("rating"))
                        chessPlayer.setRating(Double.parseDouble(info.getTextContent()));
                }
                if(i % 2 != 0 && chessPlayer.getName() != null) {
                    if(!chessPlayer.getName().equals("") && chessPlayer.getAge() != 0 &&
                    !chessPlayer.getCountry().equals("") && chessPlayer.getRating() != 0.0) {
                        chessPlayer.setType("chess");
                        try {
                            playerService.updatePlayer(chessPlayer);
                        } catch (IllegalArgumentException e) {
                            playerService.savePlayer(chessPlayer);
                        }
                    }
                }

            }

        } catch (ParserConfigurationException | SAXException | IOException ex) {
            throw new IllegalArgumentException(ex);
        }
    }

    @Override
    //@Scheduled(fixedDelay = 2592000000L)
    public void importTennisRating() {

        try {

            org.jsoup.nodes.Document doc = connect("http://tennisabstract.com/reports/atp_elo_ratings.html").get();
            final org.jsoup.nodes.Document[] doc2 = new org.jsoup.nodes.Document[1];


            Elements tdElements = doc.getElementsByAttributeValue("class", "tablesorter");

            final String[] name = new String[1];
            final String[] age = new String[1];
            final String[] rating = new String[1];
            final String[] href = new String[1];

            for (int i = 0; i < 156; i++) {
                int finalI = i;
                tdElements.forEach((Element tdElement) -> {
                    Element aElement = tdElement.child(1);
                    Element td2Element = aElement.child(finalI);
                    name[0] = td2Element.child(1).text();
                    age[0] = td2Element.child(2).text();
                    rating[0] = td2Element.child(3).text();

                    Element hrefElement = td2Element.child(1);
                    Element href2Element = hrefElement.child(0);
                    href[0] = href2Element.attr("href");
                });

                doc2[0] = Jsoup.connect(href[0]).get();

                Player tennisPlayer = new Player.Builder()
                        .name(name[0])
                        .rating(Double.parseDouble(rating[0]))
                        .age((int) Double.parseDouble(age[0]))
                        .build();
                tennisPlayer.setType("tennis");
                try {
                    playerService.updatePlayer(tennisPlayer);
                } catch (IllegalArgumentException e) {
                    playerService.savePlayer(tennisPlayer);
                }
        }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    //@Scheduled(fixedDelay = 2592000000L)
    public void importFootballRating() {
        try {
            org.jsoup.nodes.Document doc = connect("http://www.fifa.com/fifa-world-ranking/ranking-table/men/index.html").get();
            final org.jsoup.nodes.Document[] doc2 = new org.jsoup.nodes.Document[1];


            Elements tdElements = doc.getElementsByAttributeValue("class", "table tbl-ranking table-striped");

            final String[] country = new String[1];
            final String[] pos = new String[1];
            final String[] rating = new String[1];
            //final String[] href = new String[1];

            for (int i = 0; i < 211; i++)
            {
                int finalI = i;
                tdElements.forEach((Element tdElement) -> {
                    Element aElement = tdElement.child(2);
                    Element cntElement = aElement.child(finalI);
                    country[0] = cntElement.child(2).text();
                    pos[0] = cntElement.child(1).text();
                    Element ratingElement = cntElement.child(5);
                    rating[0] = ratingElement.child(0).text();
                });

                // FIXME
                Team team = new Team.Builder()
                        .country(country[0])
                        .rating(Double.parseDouble(rating[0]))
                        .build();

                try {
                    teamService.updateTeam(team);
                } catch (IllegalArgumentException e) {
                    teamService.saveTeam(team);
                }
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }
}


