package ru.ivmiit.fantasysport.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.ivmiit.fantasysport.models.Player;
import ru.ivmiit.fantasysport.models.Team;
import ru.ivmiit.fantasysport.service.interfaces.PlayerService;
import ru.ivmiit.fantasysport.service.interfaces.TeamService;

import java.util.List;

@RestController
public class PlayersController {

    @Autowired
    private PlayerService playerService;
    @Autowired
    private TeamService teamService;

    @RequestMapping(value = "/chess-players", method = RequestMethod.GET)
    public ResponseEntity<List<Player>> getChessPlayers() {
        List<Player> players = playerService.findAllChessPlayers();
        return new ResponseEntity<>(players, HttpStatus.OK);
    }

    @RequestMapping(value = "/tennis-players", method = RequestMethod.GET)
    public ResponseEntity<List<Player>> getTennisPlayers() {
        List<Player> players = playerService.findAllTennisPlayers();
        return new ResponseEntity<>(players, HttpStatus.OK);
    }

    @RequestMapping(value = "/football-teams", method = RequestMethod.GET)
    public ResponseEntity<List<Team>> getFootBallTeams() {
        List<Team> teams = teamService.findAll();
        return new ResponseEntity<>(teams, HttpStatus.OK);
    }

    @RequestMapping(value = "/find-chess-player", method = RequestMethod.POST)
    public ResponseEntity<Player> getChessPlayer(@RequestHeader("name") String name) {
        Player player = playerService.findChessPlayer(name);
        return new ResponseEntity<>(player, HttpStatus.OK);
    }

    @RequestMapping(value = "/find-tennis-player", method = RequestMethod.POST)
    public ResponseEntity<Player> getTennisPlayer(@RequestHeader("name") String name) {
        Player player = playerService.findTennisPlayer(name);
        return new ResponseEntity<>(player, HttpStatus.OK);
    }

}
