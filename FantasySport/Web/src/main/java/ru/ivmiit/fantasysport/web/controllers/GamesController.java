package ru.ivmiit.fantasysport.web.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.ivmiit.fantasysport.models.*;

@RestController
public class GamesController {

    @RequestMapping(value = "/chess-game", method = RequestMethod.POST)
    public ResponseEntity<ChessResult> startChessGame(@RequestHeader("ratingWhite") double ratingWhite,
                                                      @RequestHeader("ratingBlack") double ratingBlack) {
        Player whitePlayer = new Player.Builder()
                .rating(ratingWhite)
                .build();
        Player blackPlayer = new Player.Builder()
                .rating(ratingBlack)
                .build();

        ChessGame chessGame = new ChessGame(whitePlayer, blackPlayer);
        chessGame.start();
        ChessResult result = chessGame.getResult();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/tennis-game", method = RequestMethod.POST)
    public ResponseEntity<TennisResult> startTennisGame(@RequestHeader("ratingFirst") double ratingFirst,
                                                        @RequestHeader("ratingSecond") double ratingSecond) {
        Player firstPlayer = new Player.Builder()
                .rating(ratingFirst)
                .build();
        Player secondPlayer = new Player.Builder()
                .rating(ratingSecond)
                .build();

        TennisGame tennisGame = new TennisGame(firstPlayer, secondPlayer);
        tennisGame.start();
        TennisResult result = tennisGame.getResult();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

}
