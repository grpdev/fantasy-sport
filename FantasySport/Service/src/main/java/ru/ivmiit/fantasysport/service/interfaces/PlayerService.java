package ru.ivmiit.fantasysport.service.interfaces;

import ru.ivmiit.fantasysport.models.Player;

import java.util.List;

public interface PlayerService {

    List<Player> findAllChessPlayers();
    List<Player> findAllTennisPlayers();
    Player findChessPlayer(String name);
    Player findTennisPlayer(String name);
    void savePlayer(Player player);
    void updatePlayer(Player player);
    void deletePlayer(long id);
}
