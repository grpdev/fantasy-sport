package ru.ivmiit.fantasysport.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ivmiit.fantasysport.dao.interfaces.TeamsDao;
import ru.ivmiit.fantasysport.models.Team;
import ru.ivmiit.fantasysport.service.interfaces.TeamService;

import java.util.List;

import static ru.ivmiit.fantasysport.validators.TeamValidator.verifyTeamExistById;

@Service
public class TeamServiceImpl implements TeamService {

    @Autowired
    private TeamsDao teamsDao;

    @Override
    public List<Team> findAll() {
        return teamsDao.findAll();
    }

    @Override
    public void saveTeam(Team team) {
        teamsDao.save(team);
    }

    @Override
    public void updateTeam(Team team) {
        verifyTeamExistById(team.getId());
        teamsDao.update(team);
    }

    @Override
    public void deleteTeam(long id) {
        verifyTeamExistById(id);
        teamsDao.delete(id);
    }
}
