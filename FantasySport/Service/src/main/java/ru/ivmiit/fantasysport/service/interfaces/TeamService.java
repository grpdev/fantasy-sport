package ru.ivmiit.fantasysport.service.interfaces;

import ru.ivmiit.fantasysport.models.Team;

import java.util.List;

public interface TeamService {

    List<Team> findAll();
    void saveTeam(Team team);
    void updateTeam(Team team);
    void deleteTeam(long id);
}
