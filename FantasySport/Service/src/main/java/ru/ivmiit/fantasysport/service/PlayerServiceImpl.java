package ru.ivmiit.fantasysport.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ivmiit.fantasysport.dao.interfaces.PlayersDao;
import ru.ivmiit.fantasysport.models.Player;
import ru.ivmiit.fantasysport.service.interfaces.PlayerService;

import java.util.List;

import static ru.ivmiit.fantasysport.validators.PlayerValidator.verifyPlayerExistById;

@Service
public class PlayerServiceImpl implements PlayerService {

    @Autowired
    private PlayersDao playersDao;

    @Override
    public List<Player> findAllChessPlayers() {
        return playersDao.findAllChessPlayers();
    }

    @Override
    public List<Player> findAllTennisPlayers() {
        return playersDao.findAllTennisPlayers();
    }

    @Override
    public Player findChessPlayer(String name) {
        return playersDao.findChessPlayerByName(name);
    }

    @Override
    public Player findTennisPlayer(String name) {
        return playersDao.findTennisPlayerByName(name);
    }

    @Override
    public void savePlayer(Player player) {
        playersDao.save(player);
    }

    @Override
    public void updatePlayer(Player player) {
        verifyPlayerExistById(player.getId());
        playersDao.update(player);
    }

    @Override
    public void deletePlayer(long id) {
        verifyPlayerExistById(id);
        playersDao.delete(id);
    }
}
