package ru.ivmiit.fantasysport.models;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static ru.ivmiit.fantasysport.models.data.TestData.*;

@RunWith(MockitoJUnitRunner.class)
public class ChessGameTest {

    @InjectMocks
    private ChessGame game = new ChessGame(getWhitePlayer(), getBlackPlayer());

    private int countLoss = 0;
    private int countDraw = 0;
    private int countWin = 0;

    @Before
    public void startGames() {

        for(int i = 0; i < 100; i++) {
            game.start();
            if(game.getResult().isLoss())
                countLoss++;
            if(game.getResult().isDraw())
                countDraw++;
            if(game.getResult().isWin())
                countWin++;
        }
    }

    @Test
    public void checkRandomValue() {
        Assert.assertEquals((double) countLoss, game.getPercents()[0], 10);
        Assert.assertEquals((double) countDraw, game.getPercents()[1], 10);
        Assert.assertEquals((double) countWin, game.getPercents()[2], 10);
    }

    @Test
    public void checkProbability() {
        game.start();

        double lossProbability = game.getPercents()[0];
        double drawProbability = game.getPercents()[1];
        double winProbability = game.getPercents()[2];

        Assert.assertEquals(lossProbability, getRealLossProbability(), 0.5);
        Assert.assertEquals(drawProbability, getRealDrawProbability(), 0.5);
        Assert.assertEquals(winProbability, getRealWinProbability(), 0.5);
    }

}
