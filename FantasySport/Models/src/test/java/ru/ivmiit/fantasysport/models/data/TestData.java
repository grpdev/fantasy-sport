package ru.ivmiit.fantasysport.models.data;

import ru.ivmiit.fantasysport.models.Player;

public class TestData {

    private static final double ratingWhite = 2380;
    private static final double ratingBlack = 2150;

    private static final double realLossProbability = 9;
    private static final double realDrawProbability = 24;
    private static final double realWinProbability = 67;

    public static Player getWhitePlayer() {
        return new Player.Builder()
                .rating(ratingWhite)
                .build();
    }

    public static Player getBlackPlayer() {
        return new Player.Builder()
                .rating(ratingBlack)
                .build();
    }


    public static double getRealLossProbability() {
        return realLossProbability;
    }

    public static double getRealDrawProbability() {
        return realDrawProbability;
    }

    public static double getRealWinProbability() {
        return realWinProbability;
    }
}
