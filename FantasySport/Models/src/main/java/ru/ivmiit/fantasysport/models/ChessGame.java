package ru.ivmiit.fantasysport.models;

//@Component
//@Scope("prototype")
public class ChessGame extends Game {

    private Player white;
    private Player black;
    private double[] percents;
    private ChessResult result;
    private DrawMatrix drawMatrix = DrawMatrix.getInstance();

    public ChessGame(Player white, Player black) {
        this.white = white;
        this.black = black;
    }

    private void probability() {
        percents = new double[3];
        double ratingWhite = white.getRating();
        double ratingBlack = black.getRating();
        percents[1] = drawMatrix.getProbabilityDraw(ratingWhite, ratingBlack) * 100;
        percents[0] = black.expectedValue(ratingWhite) * 100 - percents[1] / 2;
        percents[2] = 100 - percents[0] - percents[1];
    }

    public void start() {
        if(percents == null)
            probability();

        double[] values = {0, 0.5, 1};
        double res = discreteRV(values, percents);

        if(res == 0.0)
            result = ChessResult.LOSS;
        if(res == 0.5)
            result = ChessResult.DRAW;
        if(res == 1.0)
            result = ChessResult.WIN;
    }

    public void setWhitePlayer(Player white) {
        this.white = white;
    }

    public void setBlackPlayer(Player black) {
        this.black = black;
    }

    public Player getWhitePlayer() {
        return white;
    }

    public Player getBlackPlayer() {
        return black;
    }

    public ChessResult getResult() {
        return result;
    }

    public double[] getPercents() {
        return percents;
    }
}
