package ru.ivmiit.fantasysport.models;

import java.util.Random;

public abstract class Game {

    protected double discreteRV (double[] values, double[] percents) {
        if (values == null || percents == null)
            throw new IllegalArgumentException("Input parameters are null");

        if (values.length != percents.length)
            throw new IllegalArgumentException("Input parameters length mismatch");

        for (double percent : percents) {
            if (percent < 0)
                throw new IllegalArgumentException("Negative percents are not allowed");
        }

        int rand = new Random().nextInt(100);

        double left = 0, right = 0;
        for (int i = 0; i < percents.length; i++) {
            right += percents[i];
            if (rand >= left && rand < right)
                return values[i];
            left = right;
        }

        throw new IllegalArgumentException("");
    }

    abstract void start();
}
