package ru.ivmiit.fantasysport.models;

import java.util.Random;

//@Component
//@Scope("prototype")
public class TennisGame extends Game {

    private static int MAX_SET_POINTS = 6;

    private Player first;
    private Player second;
    private double[] percents;
    private TennisResult result;
    private int firstSetScore = 0;
    private int secondSetScore = 0;
    private int whiteGameScore = 0;
    private int blackGameScore = 0;

    public TennisGame(Player first, Player second) {
        this.first = first;
        this.second = second;
    }

    private void probability() {
        percents = new double[2];
        double ratingWhite = first.getRating();
        double ratingBlack = second.getRating();
//        percents[0] = second.expectedValue(ratingWhite) * 100;
//        percents[1] = 100 - percents[0];
    }

    public void start() {
        result = calculateTennisResult();
    }

    private TennisResult calculateTennisResult() {
        if(markovChainMatch(0,0, (new Random()).nextBoolean()) > 0.5){
            return new TennisResult(true, this.firstSetScore,secondSetScore);
        } else {
            return new TennisResult(false, this.firstSetScore,secondSetScore);
        }
    }

    private double markovChainMatch(int firstSetPoints, int secondSetPoints, boolean playerOneOnServe){

        double p1SetProb = Math.abs(first.getRating() * (new Random()).nextFloat() * ((firstSetPoints - secondSetPoints + 1)/6)/first.getRating());
        double p2SetProb = Math.abs(second.getRating() * (new Random()).nextFloat() * ((firstSetPoints - secondSetPoints + 1)/6)/second.getRating());

        if(firstSetPoints == MAX_SET_POINTS) {
            firstSetScore = firstSetPoints;
            secondSetScore = secondSetPoints;
            return 1;
        } else if(secondSetPoints == MAX_SET_POINTS){
            firstSetScore = firstSetPoints;
            secondSetScore = secondSetPoints;
            return 0;
        } else if (playerOneOnServe){

            return p1SetProb * markovChainMatch(firstSetPoints + 1, secondSetPoints, !playerOneOnServe) + (1 - p1SetProb) * markovChainMatch(firstSetPoints, secondSetPoints + 1, !playerOneOnServe);
        } else {
            return p2SetProb * markovChainMatch(firstSetPoints, secondSetPoints + 1, !playerOneOnServe) + (1 - p2SetProb) * markovChainMatch(firstSetPoints + 1, secondSetPoints, !playerOneOnServe);
        }
    }

    public void setWhitePlayer(Player white) {
        this.first = white;
    }

    public void setBlackPlayer(Player black) {
        this.second = black;
    }

    public Player getWhitePlayer() {
        return first;
    }

    public Player getBlackPlayer() {
        return second;
    }

    public TennisResult getResult() {
        return result;
    }

    public double[] getPercents() {
        return percents;
    }

    private boolean isGameFinished() {
        return result != null;
    }
//
//    private void adjustGameFinished() {
//        if (whiteGameScore == 6) {
//            this.firstSetScore += 1;
//            this.whiteGameScore = 0;
//            this.blackGameScore = 0;
//        }
//        if (blackGameScore == 6) {
//            this.secondSetScore += 1;
//            this.whiteGameScore = 0;
//            this.blackGameScore = 0;
//        }
//        if (firstSetScore == 3)
//            this.result = TennisResult.WIN;
//        if (secondSetScore == 3)
//            this.result = TennisResult.LOSS;
//    }
}