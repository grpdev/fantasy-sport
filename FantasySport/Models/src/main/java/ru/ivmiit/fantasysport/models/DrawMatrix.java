package ru.ivmiit.fantasysport.models;

import java.io.*;
import java.util.StringTokenizer;

public class DrawMatrix {

    private double [][] value;
    private static DrawMatrix instance = new DrawMatrix();

    private DrawMatrix() {
        value = new double[7][16];

        String line;
        String str;
        double a;
        int i = 0;
        //String fileName = "src/main/resources/tableDraw.txt";
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("tableDraw.txt");
        try {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader in = new BufferedReader(inputStreamReader);
            while ((line = in.readLine()) != null) {
                StringTokenizer token = new StringTokenizer(line);
                str = token.nextToken();
                a = Double.parseDouble(str);
                value[i][0] = a;
                for(int j = 1; j < 16; j++) {
                    str = token.nextToken();
                    a = Double.parseDouble(str);
                    value[i][j] = a;
                }
                i++;
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static DrawMatrix getInstance() {
        return instance;
    }

    public double getProbabilityDraw(double ratingWhite, double ratingBlack) {

        double x, x1, x2, y, y1, y2;
        double fR1, fR2, fP;

        x = Math.abs(ratingBlack - ratingWhite);
        y = (ratingBlack + ratingWhite) / 2;

        int j1 = (int) Math.floor(Math.abs(ratingBlack - ratingWhite) / 20); //меньшее
        int j2 = (int) Math.ceil(Math.abs(ratingBlack - ratingWhite) / 20); //большее

        if(j1 > 15)
            j1 = 15;
        if(j1 < 0)
            j1 = 0;
        if(j2 > 15)
            j2 = 15;
        if(j2 < 0)
            j2 = 0;

        x1 = j1 * 20;
        x2 = j2 * 20;

        if(x1 == x2)
            x2 += 0.0001;

        //вычисляем значения у1,у2
        double tmp = (ratingBlack + ratingWhite) / 2;
        int i1 = (int) Math.floor(((tmp / 100 - 14) / 2));
        int i2 = (int) Math.ceil(((tmp / 100 - 14) / 2));

        if(i1 > 6)
            i1 = 6;
        if(i1 < 0)
            i1 = 0;
        if(i2 > 6)
            i2 = 6;
        if(i2 < 0)
            i2 = 0;

        y1 = 1400 + 200 * i1;
        y2 = 1400 + 200 * i2;
        if(y1 == y2)
            y2 += 0.0001;

        fR1 = (x2 - x) / (x2 - x1) * value[i1][j1] + (x - x1) / (x2 - x1) * value[i1][j2];
        fR2 = (x2 - x) / (x2 - x1) * value[i2][j1] + (x - x1) / (x2 - x1) * value[i2][j2];
        fP = (y2 - y) / (y2 - y1) * fR1 + (y - y1) / (y2 - y1) * fR2;
        return fP;

    }
}
