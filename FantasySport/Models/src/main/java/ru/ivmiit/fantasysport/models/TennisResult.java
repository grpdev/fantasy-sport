package ru.ivmiit.fantasysport.models;

public class TennisResult {

    private boolean isFirstPlayerWon;
    private int firstPlayerPoints;
    private int secondPlayerPoints;

    TennisResult(boolean isFirstPlayerWon, int firstPlayerPoints, int secondPlayerPoints) {

        this.isFirstPlayerWon = isFirstPlayerWon;
        this.firstPlayerPoints = firstPlayerPoints;
        this.secondPlayerPoints = secondPlayerPoints;
    }

    public boolean getIsFirstPlayerWon() {
        return isFirstPlayerWon;
    }

    public int getFirstPlayerPoints() { return firstPlayerPoints; }

    public  int getSecondPlayerPoints() { return secondPlayerPoints; }

    @Override
    public String toString() {
        if (this.isFirstPlayerWon) {
            return "WIN 1st:" + firstPlayerPoints + " 2st: " + secondPlayerPoints;
        } else {
            return "LOSS 1st:" + firstPlayerPoints + " 2st: " + secondPlayerPoints;
        }
    }
}