package ru.ivmiit.fsclient.models;

public enum ChessResult {

    WIN(1.0), DRAW(0.5), LOSS(0.0);

    private double value;

    ChessResult(double value) {
        this.value = value;
    }

    public boolean isWin() {
        switch (this) {
            case WIN:
                return true;
            case DRAW:
                return false;
            case LOSS:
                return false;
        }
        return false;
    }

    public boolean isDraw() {
        switch (this) {
            case WIN:
                return false;
            case DRAW:
                return true;
            case LOSS:
                return false;
        }
        return false;
    }

    public boolean isLoss() {
        switch (this) {
            case WIN:
                return false;
            case DRAW:
                return false;
            case LOSS:
                return true;
        }
        return false;
    }

    public double getValue() {
        return value;
    }

    @Override
    public String toString() {
        switch (this) {
            case WIN:
                return "WIN";
            case DRAW:
                return "DRAW";
            case LOSS:
                return "LOSS";
        }
        return "";
    }
}
