package ru.ivmiit.fsclient.models;

public class Player {

    private long id;
    private String name;
    private int age;
    private double rating;
    private String country;
    private String type;

    public Player() {}

    public Player(Builder builder) {
        setId(builder.id);
        setName(builder.name);
        setAge(builder.age);
        setRating(builder.rating);
        setCountry(builder.country);
        setType(builder.type);
    }

    public static class Builder {
        private long id;
        private String name;
        private int age;
        private double rating;
        private String country;
        private String type;

        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder age(int age) {
            this.age = age;
            return this;
        }

        public Builder rating(double rating) {
            this.rating = rating;
            return this;
        }

        public Builder country(String country) {
            this.country = country;
            return this;
        }

        public Builder type(String type) {
            this.type = type;
            return this;
        }

        public Player build() {
            return new Player(this);
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
