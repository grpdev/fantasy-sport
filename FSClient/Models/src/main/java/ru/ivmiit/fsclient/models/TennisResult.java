package ru.ivmiit.fsclient.models;

public enum TennisResult {

    WIN(1.0), LOSS(0.0);

    private double value;

    TennisResult(double value) {
        this.value = value;
    }

    public boolean isWin() {
        switch (this) {
            case WIN:
                return true;
            case LOSS:
                return false;
        }
        return false;
    }

    public boolean isLoss() {
        switch (this) {
            case WIN:
                return false;
            case LOSS:
                return true;
        }
        return false;
    }

    public double getValue() {
        return value;
    }

    @Override
    public String toString() {
        switch (this) {
            case WIN:
                return "WIN";
            case LOSS:
                return "LOSS";
        }
        return "";
    }
}
