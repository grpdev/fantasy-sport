package ru.ivmiit.fsclient.gui.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import ru.ivmiit.fsclient.models.ChessResult;
import ru.ivmiit.fsclient.models.Player;
import ru.ivmiit.fsclient.service.GameService;
import ru.ivmiit.fsclient.service.PlayerService;

import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {


    @FXML
    private JFXComboBox<String> comboBox;
    @FXML
    private JFXTextField textField;
    @FXML
    private JFXButton start;
    @FXML
    private JFXButton find;
    @FXML
    private Label resultMessage;
    @FXML
    private Label findLabel;
    @FXML
    private Label name1;
    @FXML
    private Label age1;
    @FXML
    private Label rating1;
    @FXML
    private Label country1;
    @FXML
    private Label name2;
    @FXML
    private Label age2;
    @FXML
    private Label rating2;
    @FXML
    private Label country2;

    private int game;
    private int countPlayers;
    private Player firstPlayer;
    private Player secondPlayer;
    private PlayerService playerService = PlayerService.getInstance();
    private GameService gameService = GameService.getInstance();


    @FXML
    private void findClick(Event event) {
        if(!(event instanceof ActionEvent) && !(event instanceof KeyEvent && ((KeyEvent) event).getCode().equals(KeyCode.ENTER)))
            return;
        String name = textField.getText();
        if(!name.equals("")) {
            MultiValueMap<String, String> headers = new HttpHeaders();
            headers.add("name", name);
            switch (game) {
                case 0: {
                    ResponseEntity<Player> responseEntity = playerService.getChessPlayer(headers);
                    Player player = responseEntity.getBody();
                    if(countPlayers == 0) {
                        firstPlayer = player;
                        name1.setText(player.getName());
                        age1.setText("Age: " + player.getAge());
                        rating1.setText("ELO: " + player.getRating());
                        country1.setText(player.getCountry());
                        name1.setVisible(true);
                        age1.setVisible(true);
                        rating1.setVisible(true);
                        country1.setVisible(true);
                        textField.clear();
                        countPlayers++;
                        break;
                    }
                    if(countPlayers == 1) {
                        secondPlayer = player;
                        name2.setText(player.getName());
                        age2.setText("Age: " + player.getAge());
                        rating2.setText("ELO: " + player.getRating());
                        country2.setText(player.getCountry());
                        textField.clear();
                        find.setDisable(true);
                        name2.setVisible(true);
                        age2.setVisible(true);
                        rating2.setVisible(true);
                        country2.setVisible(true);
                        countPlayers++;
                        start.setVisible(true);
                        break;
                    }

                }
                case 1: {
                    playerService.getTennisPlayer(headers);
                    break;
                }
                case 2: {

                    break;
                }
            }
        }

    }

    @FXML
    private void cancelClick() {
        if(countPlayers == 1) {
            name1.setVisible(false);
            age1.setVisible(false);
            rating1.setVisible(false);
            country1.setVisible(false);
            countPlayers--;
        }
        if(countPlayers == 2) {
            name2.setVisible(false);
            age2.setVisible(false);
            rating2.setVisible(false);
            country2.setVisible(false);
            countPlayers--;
            find.setDisable(false);
            start.setVisible(false);
        }
    }

    @FXML
    private void startClick() {
        if(countPlayers == 2) {
            switch (game) {
                case 0: {
                    MultiValueMap<String, String> headers = new HttpHeaders();
                    headers.add("ratingWhite", "" + firstPlayer.getRating());
                    headers.add("ratingBlack", "" + secondPlayer.getRating());
                    ResponseEntity<ChessResult> responseEntity = gameService.startChessGame(headers);
                    ChessResult result = responseEntity.getBody();
                    switch (result) {
                        case WIN: {
                            resultMessage.setText(firstPlayer.getName() + " wins!");
                            resultMessage.setVisible(true);
                            break;
                        }
                        case DRAW: {
                            resultMessage.setText("Draw!");
                            resultMessage.setVisible(true);
                            break;
                        }
                        case LOSS: {
                            resultMessage.setText(secondPlayer.getName() + " wins!");
                            resultMessage.setVisible(true);
                            break;
                        }
                    }
                    break;
                }
            }
        }
    }

    private void hideData() {
        name1.setVisible(false);
        age1.setVisible(false);
        rating1.setVisible(false);
        country1.setVisible(false);
        name2.setVisible(false);
        age2.setVisible(false);
        rating2.setVisible(false);
        country2.setVisible(false);
        start.setVisible(false);
        resultMessage.setVisible(false);
        find.setDisable(false);
        countPlayers = 0;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        game = 0;
        countPlayers = 0;

        comboBox.valueProperty().addListener(((observable, oldValue, newValue) -> {
            switch (newValue) {
                case "Chess" : {
                    hideData();
                    game = 0;
                    findLabel.setText("Find the players");
                    return;
                }
                case "Tennis" : {
                    hideData();
                    game = 1;
                    findLabel.setText("Find the players");
                    return;
                }
                case "Football" : {
                    hideData();
                    game = 2;
                    findLabel.setText("Find the teams");
                }
            }
        }));
    }
}
