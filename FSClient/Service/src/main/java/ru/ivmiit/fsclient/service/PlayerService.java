package ru.ivmiit.fsclient.service;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import ru.ivmiit.fsclient.models.Player;
import ru.ivmiit.fsclient.web.connection.NetworkConnection;

public class PlayerService {

    private static PlayerService instance = new PlayerService();

    private NetworkConnection networkConnection = NetworkConnection.getInstance();

    private PlayerService() {}

    public ResponseEntity<Player> getChessPlayer(MultiValueMap<String, String> headers) {
        return networkConnection.exchange("/find-chess-player", HttpMethod.POST, headers, Player.class);
    }

    public ResponseEntity<Player> getTennisPlayer(MultiValueMap<String, String> headers) {
        return networkConnection.exchange("/find-tennis-player", HttpMethod.POST, headers, Player.class);
    }

    public static PlayerService getInstance() {
        return instance;
    }
}
