package ru.ivmiit.fsclient.service;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import ru.ivmiit.fsclient.models.ChessResult;
import ru.ivmiit.fsclient.models.TennisResult;
import ru.ivmiit.fsclient.web.connection.NetworkConnection;

public class GameService {

    private static GameService instance = new GameService();

    private NetworkConnection networkConnection = NetworkConnection.getInstance();

    private GameService() {}

    public ResponseEntity<ChessResult> startChessGame(MultiValueMap<String, String> headers) {
        return networkConnection.exchange("/chess-game", HttpMethod.POST, headers, ChessResult.class);
    }

    public ResponseEntity<TennisResult> startTennisGame(MultiValueMap<String, String> headers) {
        return networkConnection.exchange("/tennis-game", HttpMethod.POST, headers, TennisResult.class);
    }

    public static GameService getInstance() {
        return instance;
    }
}
